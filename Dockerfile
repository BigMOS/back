#Image inicial base/inicial
FROM node:latest

#Crear directorio del contenedor Docker
WORKDIR /docker-api-peru

#Copiar los archivos del proyecto en el directorio del contenedor
ADD . /docker-api-peru

#Instalar dependencias de produccion    
#RUN npm install --only=production

#Exponer puerto de escucha del contenedor(miosmo definido en API REST)
EXPOSE 3000

#Lanzar comando necesarios para ejecutar nuestra API
CMD ["node", "server.js" ]
#CMD ["npm", "run", "pro" ]