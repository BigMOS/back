require('dotenv').config();
const cors = require('cors');
var express = require('express');
const userFile = require('./data/user.json');
var app = express();
var port = process.env.PORT || 3000;

const requestJSON = require('request-json');

const bodyParser = require('body-parser');
const URL_BASE = "/apiperu/v1/";
const apikeyMLab = 'apiKey=' + process.env.MLABAPIKEY;
const mLabURLbase = 'https://api.mlab.com/api/1/databases/techu18db/collections/';
const apiKey = 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF';
//const apiKey = 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF'; 

app.use(bodyParser.json())
app.use(cors());
app.options('*' ,cors());


//Servidor escuchara en la URL (servidor local)
app.listen(port, function() {
  console.log("Node JS escuchando...");
});


//Operacion GET a todos los usuarios (collections)
app.get(URL_BASE+'users',
    function(request,response) {
      response.send(userFile);
});

// Operación GET a un usuario con ID (Instance)
app.get(URL_BASE + 'users/:id',
    function(request, response) {
      let indice = request.params.id;
      let respuesta =
        (userFile[indice-1] == undefined) ? {"msg":"No existe"} : userFile[indice-1]
      response.status(200).send(respuesta);
});


// Operación GET todos los usuarios (Collections) (user.json)
app.get(URL_BASE + 'userstotal',
    function(request, response) {
      console.log(request.query);
      let total = userFile.length;
//      let totalJson = JSON.stringify({num_elem : total});
      response.status(200).send({"total": total});
});


// Operación POST a users
app.post(URL_BASE + 'users',
    function(req, res) {
      console.log(req.body);
      console.log(req.body.id);
    let newUser = {
      id: req.body.id,
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      email: req.body.email,
      password: req.body.password
    }
    userFile.push(newUser);
    res.status(201);
    res.send({"mensaje":"Usuario creado correctamente",
              "usuario":newUser,
              "userFile actualizado":userFile});
});

// pendiente PUT
app.put(URL_BASE + 'users/:id',
    function(req, res) {
      console.log(req.body);
          let newUser = {
      id: req.params.id,
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      email: req.body.email,
      password: req.body.password
    }
    userFile.push(newUser);
    res.status(200);
    res.send({"mensaje":"Usuario creado correctamente",
              "usuario":newUser,
              "userFile actualizado":userFile});
});


// DEL
app.delete(URL_BASE + 'users/:id',
function(request, response) {
  let indice = request.params.id;
//  userFile.splice(indice-1,1);
  let respuesta =
    (userFile[indice-1] == undefined) ?
      {"msg":"No existe"} :
        {"Usuario eliminado" : (userFile.splice(indice-1,1))}
  response.status(200).send(respuesta);
});


// LOGIN - user.json
app.post(URL_BASE + 'login',
  function(request, response) {
  // console.log("POST /apitechu/v1/login");
   console.log(request.body.email);
   console.log(request.body.password);
   var user = request.body.email;
   var pass = request.body.password;
   for(us of userFile) {
    if(us.email == user) {
     if(us.password == pass) {
      us.logged = true;
      writeUserDataToFile(userFile);
      console.log("Login correcto!");
      response.send({"msg" : "Login correcto.",
              "idUsuario" : us.id,
               "logged" : "true"});
     } else {
      console.log("Login incorrecto.");
      response.send({"msg" : "Login incorrecto."});
     }
    }
   }
 }
 );

  // LOGOUT - users.json
 app.post(URL_BASE + 'logout',
 function(request, response) {
  //console.log("POST /apitechu/v1/logout");
  var userId = request.body.id;
  for(us of userFile) {
   if(us.id == userId) {
    if(us.logged) {
     delete us.logged; // borramos propiedad 'logged'
     writeUserDataToFile(userFile);
     console.log("Logout correcto!");
     response.send({"msg" : "Logout correcto.", "idUsuario" : us.id});
    } else {
     console.log("Logout incorrecto.");
     response.send({"msg" : "Logout incorrecto."});
    }
   }
  }
});

function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./data/user.json", jsonUserData, "utf8",
   function(err) { //función manejadora para gestionar errores de escritura
    if(err) {
     console.log(err);
    } else {
     console.log("Datos grabados en 'user.json'.");
    }
   })
 }

// Petición GET id con mLab
app.get(URL_BASE + 'usersm/:id',
function (req, res) {
  console.log("GET /colapi/v3/users/:id");
  console.log(req.params.id);
  var id = req.params.id;
  var queryString = 'q={"usuarioID":' + id + '}&';
  var queryStrField = 'f={"_id":0}&';
  var httpClient = requestJSON.createClient(mLabURLbase);
  httpClient.get('user?' + queryString + queryStrField + apikeyMLab,
    function(err, respuestaMLab, body){
      //console.log("Respuesta mLab correcta.");
      //console.log(respuestaMLab);
      console.log("Body");
      console.log(body);
      // console.log("Response");
      // console.log(response);
      console.log("err");
      console.log(err);
    //  var respuesta = body[0];
      var response = {};
      if(err) {
          response = {"msg" : "Error obteniendo usuario."}
          res.status(500);
      } else {
        if(body.length > 0) {
          response = body;
        } else {
          response = {"msg" : "Usuario no encontrado."}
          res.status(404);
        }
      }
      res.send(response);
    });
});

