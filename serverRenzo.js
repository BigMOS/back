// No se si usaron controllers
// Pero aqui el ejemplo del archivo server.js
var express = require('express');
var bodyParser = require('body-parser');
var app = express();

require('dotenv').config();
const apiKey = process.env.MLAB_API_KEY;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
var requestJSON = require('request-json');
const URL_BASE = '/apitechu/v3/';
const URL_MLAB = 'https://api.mlab.com/api/1/databases/techu19db/collections/';
const PORT = process.env.PORT || 3003;

const logout_controller = require('./controllers/logout_controller');
const login_controller = require('./controllers/login_controller');

//POST LOGIN
// curl -s -H 'Content-Type: application/json' -X POST http://localhost:3003/apitechu/v3/login -d '{"email" : "rportocarrero@bbva.com", "password" : "pepito"}' | jq
app.post(URL_BASE + 'login', login_controller.postLogin);

//POST LOGOUT
// curl -s -H 'Content-Type: application/json' -X POST http://localhost:3003/apitechu/v3/logout -d '{"email" : "rportocarrero@bbva.com", "password" : "pepito"}' | jq
app.post(URL_BASE + 'logout', logout_controller.postLogout);

app.listen(PORT, function () {
 console.log('API escuchando en el puerto 3003...');
});

//La del login
//Login:
//cat controllers/login_controller.js
module.exports.postLogin = postLogin;
var requestJSON = require('request-json');
const URL_BASE = '/apitechu/v3/';
const URL_MLAB = 'https://api.mlab.com/api/1/databases/techu19db/collections/';

require('dotenv').config();
const apiKey = process.env.MLAB_API_KEY;

 function postLogin(req, res){
 let email = req.body.email;
 let pass = req.body.password;
 let queryString = 'q={"email":"' + email + '","password":"' + pass + '"}&';
 let limFilter = 'l=1&';
 let httpClient = requestJSON.createClient(URL_MLAB);
 httpClient.get('user?'+ queryString + limFilter + 'apiKey=' + apiKey,
   function(err, respuestaMLab, body) {
    console.log(URL_MLAB, 'user?' + queryString + limFilter + 'apiKey=' + apiKey);
    console.log(err, body);
     if(!err) {
       if (body.length == 1) { // Existe un usuario que cumple 'queryString'
         let login = '{"$set":{"logged":true}}';
         httpClient.put('user?q={"id": ' + body[0].id + '}&' + 'apiKey=' + apiKey, JSON.parse(login),
         //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
           function(errPut, resPut, bodyPut) {
             res.send({'msg':'Login correcto', 'user':body[0].email, 'userid':body[0].id});
             // If bodyPut.n == 1, put de mLab correcto
           });
       }
       else {
         res.status(404).send({"msg":"Usuario no válido."});
       }
     } else {
       res.status(500).send({"msg": "Error en petición a mLab."});
     }
 });
};

// La del Logout:
// cat controllers/logout_controller.js
module.exports.postLogout = postLogout;
var requestJSON = require('request-json');
const URL_BASE = '/apitechu/v3/';
const URL_MLAB = 'https://api.mlab.com/api/1/databases/techu19db/collections/';

require('dotenv').config();
const apiKey = process.env.MLAB_API_KEY;

 function postLogout(req, res){
 let email = req.body.email;
 let pass = req.body.password;
 let queryString = 'q={"email":"' + email + '","password":"' + pass + '"}&';
 let limFilter = 'l=1&';
 let httpClient = requestJSON.createClient(URL_MLAB);
 httpClient.get('user?'+ queryString + limFilter + 'apiKey=' + apiKey,
   function(err, respuestaMLab, body) {
    console.log(URL_MLAB, 'user?' + queryString + limFilter + 'apiKey=' + apiKey);
    console.log(err, body);
     if(!err) {
       if (body.length == 1) { // Existe un usuario que cumple 'queryString'
         let logout = '{"$unset":{"logged":true}}';
         httpClient.put('user?q={"id": ' + body[0].id + '}&' + 'apiKey=' + apiKey, JSON.parse(logout),
         //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(logout),
           function(errPut, resPut, bodyPut) {
             res.send({'msg':'LogOut correcto', 'user':body[0].email, 'userid':body[0].id});
             // If bodyPut.n == 1, put de mLab correcto
           });
       }
       else {
         res.status(404).send({"msg":"Usuario no válido."});
       }
     } else {
       res.status(500).send({"msg": "Error en petición a mLab."});
     }
 });
};
fijate que para el login usas:     let login = '{"$set":{"logged":true}}';
Para el logout usas:           let logout = '{"$unset":{"logged":true}}';


//********** */
//user.json

[{"userID":"1","name":"Consuela","lastName":"Phillott","email":"cphillott0@mysql.com","password":"4nPcNggbAJa"},{"userID":"2","name":"Wilhelmina","lastName":"Maleck","email":"wmaleck1@artisteer.com","password":"zQtNeETph"}]
server2.js
// LOGIN CON POS
app.post(URL_BASE + 'login', function(req, res){


let userFound = false;

for (let user of userFile) {
if(user.email == req.body.email && req.body.password == user.password){
userFound = true;
user.logged = true;
writeUserDataToFile(userFile);
}
if(userFound){
console.log("Login OK");
res.send({mensaje : "Login correcto", "idUsuario" : "" + user.userID});
break;
}
}
if(!userFound){
console.log("Login failed");
res.send({mensaje : "Login incorrecto"});
}

});

// LOGOUT CON POST
app.post(URL_BASE + 'logout', function(req, res) {

let userFound = false;

for (let user of userFile) {
if(user.userID == req.body.userID && user.logged) {
userFound = true;
delete user.logged;
writeUserDataToFile(userFile);
}
if(userFound) {
console.log("logout OK");
res.send({mensaje : "Logout correcto", "idUsuario" : "" + user.userID});
break;
}
}
if(!userFound) {
console.log("Logout failed");
res.send({mensaje : "Logout incorrecto"});
}


});

// Validación:
// curl -s -H 'Content-Type: application/json' -X POST http://localhost:3001/apitechu/v2/login -d '{"email":"cphillott0@mysql.com","password":"4nPcNggbAJa"}'
// curl -s -H 'Content-Type: application/json' -X POST http://localhost:3001/apitechu/v2/logout -d '{"userID" : "1"}'

const express = require('express');
const bodyParser = require('body-parser');
var fs = require('fs');
var userFile = require('./user.json');

const app = express();

const URL_BASE = '/apitechu/v2/';

const PORT = process.env.PORT || 3001;


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//Escucha de puerto
app.listen(PORT, function () {
console.log('APITechU escuchando desde Peru en puerto 3001');
});

// LOGIN CON POS
app.post(URL_BASE + 'login', function(req, res){

let userFound = false;

for (let user of userFile) {
if(user.email == req.body.email && req.body.password == user.password){
userFound = true;
user.logged = true;
writeUserDataToFile(userFile);
}
if(userFound){
console.log("Login OK");
res.send({mensaje : "Login correcto", "idUsuario" : "" + user.userID});
break;
}
}
if(!userFound){
console.log("Login failed");
res.send({mensaje : "Login incorrecto"});
}

});

// LOGOUT CON POST
app.post(URL_BASE + 'logout', function(req, res) {

let userFound = false;

for (let user of userFile) {
if(user.userID == req.body.userID && user.logged) {
userFound = true;
delete user.logged;
writeUserDataToFile(userFile);
}
if(userFound) {
console.log("logout OK");
res.send({mensaje : "Logout correcto", "idUsuario" : "" + user.userID});
break;
}
}
if(!userFound) {
console.log("Logout failed");
res.send({mensaje : "Logout incorrecto"});
}


});


