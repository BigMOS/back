var express = require('express');
var userFile = require('./data/user.json');
var app = express();
var port = process.env.PORT || 3000;

const bodyParser = require('body-parser');
const URL_BASE = "/apiperu/v1/";

//Servidor escuchara en la URL (servidor local)
app.listen(port, function() {
  console.log("Node JS escuchando...");
});

app.use(bodyParser.json())

//Operacion GET a todos los usuarios (collections)
app.get(URL_BASE+'users',
    function(request,response) {
      response.send(userFile);
});

// Operación GET a un usuario con ID (Instance)
app.get(URL_BASE + 'users/:id',
    function(request, response) {
      let indice = request.params.id;
      let respuesta =
        (userFile[indice-1] == undefined) ? {"msg":"No existe"} : userFile[indice-1]
      response.status(200).send(respuesta);
});


// Operación GET todos los usuarios (Collections) (user.json)
app.get(URL_BASE + 'userstotal',
    function(request, response) {
      console.log(request.query);
      let total = userFile.length;
//      let totalJson = JSON.stringify({num_elem : total});
      response.status(200).send({"total": total});
});


// Operación POST a users
app.post(URL_BASE + 'users',
    function(req, res) {
      console.log(req.body);
      console.log(req.body.id);
    let newUser = {
      id: req.body.id,
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      email: req.body.email,
      password: req.body.password
    }
    userFile.push(newUser);
    res.status(201);
    res.send({"mensaje":"Usuario creado correctamente",
              "usuario":newUser,
              "userFile actualizado":userFile});
});

// pendiente PUT
app.put(URL_BASE + 'users/:id',
    function(req, res) {
      console.log(req.body);
          let newUser = {
      id: req.params.id,
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      email: req.body.email,
      password: req.body.password
    }
    userFile.push(newUser);
    res.status(200);
    res.send({"mensaje":"Usuario creado correctamente",
              "usuario":newUser,
              "userFile actualizado":userFile});
});


// DEL
app.delete(URL_BASE + 'users/:id',
function(request, response) {
  let indice = request.params.id;
//  userFile.splice(indice-1,1);
  let respuesta =
    (userFile[indice-1] == undefined) ?
      {"msg":"No existe"} :
        {"Usuario eliminado" : (userFile.splice(indice-1,1))}
  response.status(200).send(respuesta);
});

